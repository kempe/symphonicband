from django.contrib.auth import authenticate, login
from django.http import HttpResponse
from django.template import loader

def user_login(request):
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            return HttpResponse(f"{user} has been logged in")
        else:
            failed_template = loader.get_template('authenticate/login_failed.html')
            return HttpResponse(failed_template.render({}, request))
    else:
        login_template = loader.get_template('authenticate/user_login.html')
        return HttpResponse(login_template.render({}, request))

